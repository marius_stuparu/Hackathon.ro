<?php
/**
 * The loop that displays a single post.
 *
 * The loop displays the posts and the post content. See
 * http://codex.wordpress.org/The_Loop to understand it and
 * http://codex.wordpress.org/Template_Tags to understand
 * the tags used in it.
 *
 * @package WordPress
 * @subpackage PowerConsult
 */
?>
<?php if (function_exists('qtrans_getLanguage')) : ?>
	<div id="breadcrumb">
		<?php breadcrumb_trail(array(
			'separator' => '&raquo;',
			'before' => false,
			'after' => false,
			'front_page' => false,
			'show_home' => __( '[:ro]Prima pagină[:en]Home', $textdomain ),
			'echo' => true
		));
		?>
	</div>
	
<?php endif;
	// Read testimonial, store it for later
	// (because otherwise it messes with the image name)
	$args = array( 'post_type' => 'testimoniale', 'posts_per_page' => 1, 'orderby' => 'rand' );
	$testimonial = new WP_Query( $args );
	while ( $testimonial->have_posts() ) : $testimonial->the_post();
		$testimonial_text = get_the_content();
		$testimonial_autor = get_the_title();
	endwhile;
	rewind_posts();
	
	// Now let's show the page
	if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<h1 class="entry-title"><?php the_title(); ?></h1>
		<section>
			<?php the_content(); ?>
		</section>
		<?php edit_post_link( __( 'Editează', 'powerconsult' ), '<span class="edit-link">', '</span>' ); ?>
	</article>
<?php
	endwhile; // end of the page loop.
?>

<aside>
	<blockquote>
		<?php echo $testimonial_text; ?>
	</blockquote>
	<p class="nume"><?php echo $testimonial_autor ?></p>
</aside>