<?php
/**
 * @package WordPress
 * @subpackage PowerConsult
 */

get_header();

if ( is_page(764) ) {
	get_template_part( 'projects', 'public' );
} else if ( is_page(767) ) {
	get_template_part( 'projects', 'voting' );
} else {
	get_template_part( 'content', 'page' );
}

get_footer();
?>
