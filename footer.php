<?php
/**
 * The template for displaying the footer.
 *
 * @package Hackathon
 * @subpackage Hackover
 */
?>
    </div> <!-- #content -->
    
    <footer>
        <p role="note"><?php echo hackathon_copyright(); ?></p>
    </footer><!-- #footer -->
	
	<!-- Google campaign code -->
    <?php if( is_page(111) ): // confirmation page has the conversion code ?>
	    <script type="text/javascript">
            /* <![CDATA[ */
            var google_conversion_id = 963408420;
            var google_conversion_language = "en";
            var google_conversion_format = "1";
            var google_conversion_color = "ffffff";
            var google_conversion_label = "nxF7CJyVwAQQpOSxywM";
            var google_conversion_value = 0;
            /* ]]> */
        </script>
        <script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js"></script>
        <noscript>
            <div style="display:inline;">
            <img height="1" width="1" style="border-style:none;" alt="" src="http://www.googleadservices.com/pagead/conversion/963408420/?value=0&amp;label=nxF7CJyVwAQQpOSxywM&amp;guid=ON&amp;script=0"/>
            </div>
        </noscript>
    <?php else: // all other pages have the remarketing code ?>
         <script type="text/javascript">
            /* <![CDATA[ */
            var google_conversion_id = 963408420;
            var google_conversion_label = "rEFVCJSWwAQQpOSxywM";
            var google_custom_params = window.google_tag_params;
            var google_remarketing_only = true;
            /* ]]> */
        </script>
        <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js"></script>
        <noscript>
            <div style="display:inline;">
            <img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/963408420/?value=0&amp;label=rEFVCJSWwAQQpOSxywM&amp;guid=ON&amp;script=0"/>
            </div>
        </noscript>
    <?php endif; ?> 
	
	<!-- jQuery -->
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="<?php bloginfo( 'template_url' ); ?>/js/vendor/jquery-1.8.0.min.js"><\/script>')</script>

    <script src="<?php bloginfo( 'template_url' ); ?>/js/vendor/bootstrap.min.js"></script>

    <script src="<?php bloginfo( 'template_url' ); ?>/js/plugins.js"></script>
    <script src="<?php bloginfo( 'template_url' ); ?>/js/vendor/sugar-1.3.4.min.js"></script>
    <script src="<?php bloginfo( 'template_url' ); ?>/js/main.js"></script>
    
    <script>
        var _gaq=[['_setAccount','UA-35232031-1'],['_trackPageview']];
        (function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
        g.src=('https:'==location.protocol?'//ssl':'//www')+'.google-analytics.com/ga.js';
        s.parentNode.insertBefore(g,s)}(document,'script'));
    </script>
	    
	<?php wp_footer(); ?> <!-- end all footer scripts -->
</body>
</html>