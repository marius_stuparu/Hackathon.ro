/*global yepnope:true, URL:true, qrcode:true */
(function($) {
    $.qrScanner = function(options) {
        var plugin = this;
        plugin.settings = {}

        var defaults = {
            webqrLocation: 'js/webqr/',
            videoID: 'qr-video',
            canvasID: 'qr-canvas'
        }

        var requestAnimationFrame, v, canvas, gCtx, codeHit = false;

        plugin.init = function() {
            plugin.settings = $.extend({}, defaults, options);

            var webqrLocation = plugin.settings.webqrLocation;

            if( !webqrLocation.match(/\/$/g) ) {
              webqrLocation += '/'
            }

            // Load WebQR dependencies then run
            yepnope([{
              load: [
                webqrLocation + 'grid.js',
                webqrLocation + 'version.js',
                webqrLocation + 'detector.js',
                webqrLocation + 'formatinf.js',
                webqrLocation + 'errorlevel.js',
                webqrLocation + 'bitmat.js',
                webqrLocation + 'datablock.js',
                webqrLocation + 'bmparser.js',
                webqrLocation + 'datamask.js',
                webqrLocation + 'rsdecoder.js',
                webqrLocation + 'gf256poly.js',
                webqrLocation + 'gf256.js',
                webqrLocation + 'decoder.js',
                webqrLocation + 'QRCode.js',
                webqrLocation + 'findpat.js',
                webqrLocation + 'alignpat.js',
                webqrLocation + 'databr.js'
              ],
              complete: function() {
                main();
              }
            }])
        }

        plugin.validateCode = function(qrtext) {
          if( isValidCode(qrtext) ) {
            codeHit = true;
            window.location.search = '?a=list&i=' + qrtext;
          } else {
            return false;
          }
        }

        var main = function() {
          requestAnimationFrame = window.requestAnimationFrame || 
                                  window.mozRequestAnimationFrame || 
                                  window.webkitRequestAnimationFrame || 
                                  window.msRequestAnimationFrame;

          window.requestAnimationFrame = requestAnimationFrame;

          startScan();
        }

        var startScan = function() {
          var videoID = plugin.settings.videoID,
              canvasID = plugin.settings.canvasID;

          v = document.getElementById(videoID);
          canvas = document.getElementById(canvasID);
          gCtx = canvas.getContext('2d');
          navigator.webkitGetUserMedia({video:true}, callbackStreamIsReady, callbackFail);
        }

        var callbackStreamIsReady = function(stream) {
          v.src = URL.createObjectURL(stream);
          v.play();
          window.requestAnimationFrame(draw);
        }

        var callbackFail = function() {
          throw 'No camera access!';
        }

        var draw = function() {
          var w = canvas.clientWidth,
              h = canvas.clientHeight;

          try {
            gCtx.drawImage(v, 0, 0, w, h);

            window.requestAnimationFrame(draw);

            if( !codeHit ) {
              qrcode.callback = plugin.validateCode;
              qrcode.decode();
            }
          } catch(e) {
            if ( e.message === 'Couldn\'t find enough finder patterns' ){
              // no code visible yet
              return
            }
          }
        }

        var isValidCode = function(qrtext) {
          var regexp = /21042013(\d){3}hackathon/g;

          if( regexp.test(qrtext) ) {
            return true;
          } 
          
          return false;
        }

        plugin.init();
    }

    $.fn.qrScanner = function(options) {
        return this.each(function() {
            if (undefined === $(this).data('qrScanner')) {
                var plugin = new $.qrScanner(this, options);
                $(this).data('qrScanner', plugin);
            }
        });
    }
})(jQuery);