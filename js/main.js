// Author: Marius Stuparu
/*global yepnope:true */
jQuery(function($){
    var location = 'http://hackathon.ro/wp-content/themes/hackathon/';

    var processRegistrationForm = function(){
        $('input.ykfmc-submit').hide();

        $('#ykfmc-agree').on('change', function(){
            var checked = $('#ykfmc-agree').attr('checked')

            if ( checked === 'checked' ) {
                $('input.ykfmc-submit').show();
            } else {
                $('input.ykfmc-submit').hide();
            }
        })

        if( $('body').hasClass('lang-en') ) {
            $('label[for="a0176c5370-FNAME"]').text('First name')
            $('label[for="a0176c5370-LNAME"]').text('Last name')
            $('label[for="a0176c5370-PHONE"]').text('Phone')
            $('label[for="a0176c5370-PCAT"]').text('Project category')
            $('label[for="a0176c5370-TRICOU"]').text('T-shirt')
        }
    }

    var animateBars = function(maxPoints) {
        var $bars = $('#charts .bar')

        $bars.each(function(){
            var $that = $(this),
                width = $that.attr('data-width'),
                percent = (width / maxPoints) * 100;

            $that.animate({
                'width': percent + '%'
            }, 1500)

            if( 100 === percent ) {
                $that.addClass('maximum')
            }
        })
    }

    var processListaProiecte = function() {
        var totalPoints

        $('.project').on('click', '.more a', function(e){
            e.preventDefault()

            $(this)
                .parent()
                .toggleClass('close')
                .siblings('.description')
                .slideToggle('fast')
        })

        totalPoints = $('#charts .bar_container:first-child').find('.bar').attr('data-width')

        animateBars(totalPoints)
    }

    var startScanner = function(){
        yepnope([{
            load: location + 'js/vendor/camcanvas.js',
            complete: function() {
                $.qrScanner({
                    webqrLocation: location + 'js/webqr/'
                })
            }
        }])
    }

    var processVoting = function() {
        var user = {
            vote_1: null,
            vote_2: null,
            vote_3: null
        }
        
        $('body')
            .on('contextmenu', function() {
                return false
            })
            // .on('keydown', function(e) {
            //     // Block F11 and F12
            //     if( (123 ==== e.keyCode) || (122 ==== e.keyCode) ) {
            //         return false
            //     }
            // })

        $('#reset').on('click', 'span', function(){
            $('input[type="radio"]')
                .removeAttr('checked')
                .removeAttr('disabled')

            user = {
                vote_1: null,
                vote_2: null,
                vote_3: null
            }

            $('input[type="submit"]').attr('disabled', 'disabled')
        })

        $('.project')
            .on('click', '.more a', function(e){
                e.preventDefault()

                $(this)
                    .parent()
                    .toggleClass('close')
                    .siblings('.description')
                    .slideToggle('fast')
            })
            .on('change', 'input[type="radio"]', function(){
                var $that = $(this),
                    checked = $that.attr('checked'),
                    thisID = $that.attr('id'),
                    $siblings = $that.parent().parent().find('input[type="radio"]'),
                    $allInputs = $('form input[type="radio"]'),
                    voteData = [], userData = []

                if ( 'checked' === checked ) {
                    voteData = thisID.split('_');

                    switch( voteData[2] ) {
                        case '1': user.vote_1 = voteData[1]; break;
                        case '2': user.vote_2 = voteData[1]; break;
                        case '3': user.vote_3 = voteData[1]; break;
                    }

                    $siblings.each(function(){
                        if( thisID !== $(this).attr('id') ) {
                            $(this).attr('disabled', 'disabled')
                        }
                    })

                    userData = Object.values(user);

                    $allInputs.each(function(){
                        if( !userData.some( $(this).val() ) ) {
                            $(this).removeAttr('disabled')
                        }
                    })
                }

                if( userData.exclude(null).count() === 3 ) {
                    $('input[type="submit"]').removeAttr('disabled')
                } else {
                    $('input[type="submit"]').attr('disabled', 'disabled')
                }
            })
    }
    
    var Init = function(){
        if ( $('form[name="yks-mailchimp-form"]').length ) {
            processRegistrationForm()
        }
 
        if ( $('body').hasClass('lista_proiecte') ) {
            processListaProiecte()
        }

        if ( $('body').hasClass('voting_proiecte') ) {
            if( $('#scanner').length ) {
                startScanner()
            }

            if( $('form#projects_list').length ) {
                processVoting()
            }
        }
    }
    
    Init();
})
