<?php
/**
 * @package Hackathon
 * @subpackage Hackover
 */
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7"  dir="ltr" lang="ro-RO"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8"  dir="ltr" lang="ro-RO"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie10 lt-ie9"  dir="ltr" lang="ro-RO"> <![endif]-->
<!--[if IE 9]>    <html class="no-js lt-ie10"  dir="ltr" lang="ro-RO"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js" dir="ltr" lang="ro-RO"> <!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

	<title><?php
		/*
		 * Print the <title> tag based on what is being viewed.
		 */
		global $page, $paged;

		// Add the blog name.
		bloginfo( 'name' );

		// Add the blog description for the home/front page.
		$site_description = get_bloginfo( 'description', 'display' );
		if ( $site_description && ( is_home() || is_front_page() ) )
			echo " | $site_description";

		// Add a page number if necessary:
		if ( $paged >= 2 || $page >= 2 )
			echo ' | ' . sprintf( __( 'Pagina %s', 'hackathon' ), max( $paged, $page ) );
	?></title>

	<meta name="author" content="Hackathon.ro">
	<meta name="viewport" content="width=device-width">

	<link rel="profile" href="http://gmpg.org/xfn/11" />

	<!-- now the stylesheets -->
	<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
	<link rel="stylesheet" type="text/css" media="screen" href="<?php bloginfo( 'template_url' ); ?>/css/layout.css?v=2" />
	<!-- Human readable stylesheet can be found here: <?php bloginfo( 'template_url' ); ?>/sass/layout.scss -->

	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />

	<!-- Modernizr -->
	<script src="<?php bloginfo( 'template_url' ); ?>/js/vendor/modernizr-2.6.1-respond-1.1.0.min.js"></script>

	<!-- all other header loads -->
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
    <?php
	    if( is_page(767) ) :
	    	require_once 'header_voting.php';
	    else :
	    	require_once 'header_home.php';
    ?>

    <nav>
        <div class="wrapper">
        	<?php
	            wp_nav_menu( array(
	                'theme_location' => 'primary',
	                'items_wrap' => '<ul>%3$s</ul>',
	            ) );
	        ?>
        </div>
    </nav>

	<?php endif; ?>

    <div id="content">