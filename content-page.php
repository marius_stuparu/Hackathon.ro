<?php
/**
 * @package Hackathon
 * @subpackage Hackover
 */
    $sidebar = checkPage();
    
    while ( have_posts() ) : the_post();
?>
    <div id="main_content" class="<?php if ($sidebar == 1) { echo 'gold'; } else { echo 'full'; } ?>">
        <?php 
            if (function_exists('HAG_Breadcrumbs')) { 
                HAG_Breadcrumbs(array(
                    'home_label' => 'Hackathon.ro',
                    'crumb_class' => 'item',
                    'crumb_element' => 'span',
                    'separator' => '<span class="separator">&raquo;</span>'
                    )); 
            }

            the_content();
        ?>
    </div>
<?php 
    endwhile;
    
    if ($sidebar == 1) :
?>
    <div id="sidebar">
        <?php dynamic_sidebar( 'primary-widget-area' ) ?>
    </div>
<?php endif; ?>