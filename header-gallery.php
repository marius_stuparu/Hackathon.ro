<?php
/**
 * @package Hackathon
 * @subpackage Hackover
 */
?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7"  dir="ltr" lang="ro-RO"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8"  dir="ltr" lang="ro-RO"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9"  dir="ltr" lang="ro-RO"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" dir="ltr" lang="ro-RO"> <!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

	<title><?php
		/*
		 * Print the <title> tag based on what is being viewed.
		 */
		global $page, $paged;		
	
		// Add the blog name.
		bloginfo( 'name' );
	
		// Add the blog description for the home/front page.
		$site_description = get_bloginfo( 'description', 'display' );
		if ( $site_description && ( is_home() || is_front_page() ) )
			echo " | $site_description";
	
		// Add a page number if necessary:
		if ( $paged >= 2 || $page >= 2 )
			echo ' | ' . sprintf( __( 'Pagina %s', 'hackathon' ), max( $paged, $page ) );
	?></title>
	
	<meta name="author" content="Hackathon.ro">
	<meta name="viewport" content="width=device-width">

	<link rel="profile" href="http://gmpg.org/xfn/11" />
	
	<!-- load Ubuntu for main text, CabinSketch for headers -->
	<script type="text/javascript">
	  WebFontConfig = {
	    google: { families: [ 'Ubuntu:300,400,700,300italic,400italic,700italic:latin,greek-ext,greek,latin-ext' ] }
	  };
	  (function() {
	    var wf = document.createElement('script');
	    wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
	      '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
	    wf.type = 'text/javascript';
	    wf.async = 'true';
	    var s = document.getElementsByTagName('script')[0];
	    s.parentNode.insertBefore(wf, s);
	  })();
	</script>
	
	<!-- now the stylesheets -->
	<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
	<link rel="stylesheet" type="text/css" media="screen" href="<?php bloginfo( 'template_url' ); ?>/css/layout.css" />
	<!-- Human readable stylesheet can be found here: <?php bloginfo( 'template_url' ); ?>/less/layout.less -->
	
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
	
	<!-- Modernizr --> 
	<script src="<?php bloginfo( 'template_url' ); ?>/js/vendor/modernizr-2.6.1-respond-1.1.0.min.js"></script>
	
	<!-- all other header loads -->
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
    <div id="wrapper">
        <?php require_once 'header_minimal.php'; ?>
        
        <div id="content">