<?php
/**
 * @package Hackathon
 * @subpackage Hackover
 */

$options = get_option('hackathon');

$header_bg = $options['colour_header_bg'];
$header_text= $options['colour_header_text'];

$partner_logo = $options['logo_main_partner'];
$partner_text = $options['text_main_partner'];
$partner_text_pos = $options['text_main_partner_pos'];
$partner_url = $options['url_main_partner'];
?>

<style>
	header  {
		background:<?php echo $header_bg ?>;
		color:<?php echo $header_text ?>;
	}

	header a,
	header a:hover,
	header a:visited {
		color:<?php echo $header_text ?>;
	}
</style>

<header>
	<div class="wrapper">
		<h1 class="logo">
			hackathon<span>.ro</span>
		</h1>

		<?php
			// if (function_exists('qts_language_menu')) {
			// 	echo '<div id="language">';
			// 	qts_language_menu('image');
			// 	echo '</div>';
			// }
		?>
	</div>
</header>