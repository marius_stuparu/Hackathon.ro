<?php
/**
 * The loop that displays a page.
 *
 * The loop displays the posts and the post content. See
 * http://codex.wordpress.org/The_Loop to understand it and
 * http://codex.wordpress.org/Template_Tags to understand
 * the tags used in it.
 *
 * @package WordPress
 * @subpackage PowerConsult
 */
?>
<?php if (function_exists('qtrans_getLanguage')) : ?>
	<div id="breadcrumb">
		<?php breadcrumb_trail(array(
			'separator' => '&raquo;',
			'before' => false,
			'after' => false,
			'front_page' => false,
			'show_home' => __( '[:ro]Prima pagină[:en]Home', $textdomain ),
			'echo' => true
		));
		?>
	</div>
	
<?php endif;
	// We're featuring the latest 4 posts
	query_posts('showposts=4');
	$ids = array();
	
	if ( have_posts() ) :
	echo '<div class="featured clearfix">';
	
	// Let's loop them 4
	while (have_posts()) : the_post();
		$ids[] = get_the_ID();
	?>
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<p class="meta"><?php powerconsult_posted_on(); ?></p>
			<h1 class="entry-title"><?php
				printf('<a href="%1$s" title="%2$s" rel="bookmark">%3$s</a>',
					get_permalink(),
					esc_attr__( '[:ro]Legătura către %s[:en]Link to %s', 'powerconsult' ), the_title_attribute( 'echo=0' ),
					get_the_title()
				) ?></h1>
			<section>
				<?php the_excerpt(); ?>
			</section>
		</article>
<?php
	endwhile;
	
	echo '</div>';
	endif;
	
	// Now let's get the rest
	query_posts(array('post__not_in' => $ids));
	
	if ( have_posts() ) :
		echo '<ul class="all_news">';
			while (have_posts()) : the_post();
				printf('<li><span class="meta">%1$s</span><a href="%2$s" title="%3$s" rel="bookmark">%4$s</a></li>',
					powerconsult_posted_on_return(),
					get_permalink(),
					esc_attr__( '[:ro]Legătura către %s[:en]Link to %s', 'powerconsult' ), the_title_attribute( 'echo=0' ),
					get_the_title()
				);
			endwhile;
		echo '</ul>';
	endif;
?>