<?php
/**
 * @package Hackathon
 * @subpackage Hackover
 */

$options = get_option('hackathon');

$header_bg = $options['colour_header_bg'];
$header_text= $options['colour_header_text'];

$partner_logo = $options['logo_main_partner'];
$partner_text = $options['text_main_partner'];
$partner_text_pos = $options['text_main_partner_pos'];
$partner_url = $options['url_main_partner'];
?>

<style>
	header  {
		background:<?php echo $header_bg ?>;
		color:<?php echo $header_text ?>;
	}

	header a,
	header a:hover,
	header a:visited {
		color:<?php echo $header_text ?>;
	}
</style>

<header>
	<div class="wrapper">
		<h1 class="logo">
			<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">hackathon<span>.ro</span></a>
		</h1>

		<?php if ( $partner_logo ) : ?>
		<div id="main_partner">
			<a href="<?php echo $partner_url ?>"><?php
				if ( $partner_text_pos == 1 ) {
					printf($partner_text . '<img src="' . $partner_logo . '" />');
				} else {
					printf('<img src="' . $partner_logo . '" />' . $partner_text);
				}
			?>
		</div>
		<?php endif ?>
	</div>
</header>

<div class="raspberry_hack">
	<div class="wrapper">
		<h2>raspberry</h2>
		<h1>HACK</h1>
		<h3>20&mdash;21 aprilie 2013</h3>
		<h4>Crystal Palace Ballrooms &mdash; București</h4>
		<!--<p class="register">
<a href="<?php // echo get_permalink(519) ?>" class="button"><?php /* if (function_exists('qtrans_getLanguage')) {
			echo __('[:ro]Înscrie-te acum! &mdash; Locurile sunt limitate[:en]Register now! &mdash; Seats are limited', 'hackathon');
			} else { echo __('Înscrie-te acum! &mdash; Locurile sunt limitate', 'hackathon'); } */ ?></a>
		</p> -->
		<p class="raspberry"><a href="http://www.raspberrypi.org" target="_blank"><img src="<?php bloginfo( 'template_url' ); ?>/img/raspberry-pi-logo-black.png" /></a></p>
	</div>
</div>