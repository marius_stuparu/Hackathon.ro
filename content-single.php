<?php
/**
 * @package Hackathon
 * @subpackage Hackover
 */

    while ( have_posts() ) : the_post();
?>
    <div class="row-fluid">
        <div class="span8">
            <h3><a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__( 'Legătura către %s', 'hackathon' ), the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark"><?php the_title(); ?></a></h3>
            <?php the_content() ?>
        </div>
<?php 
    endwhile;
?>
        <div class="span4">
            <p><a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="btn btn-primary btn-block btn-large">Înscrie-te acum!</a></p>
            <?php dynamic_sidebar( 'secondary-widget-area' ) ?>
        </div>
    </div>