<div id="main_content" class="<?php if ($sidebar == 1) { echo 'gold'; } else { echo 'full'; } ?>">
	<?php
if (function_exists('HAG_Breadcrumbs')) {
    HAG_Breadcrumbs(array(
        'home_label' => 'Hackathon.ro',
        'crumb_class' => 'item',
        'crumb_element' => 'span',
        'separator' => '<span class="separator">&raquo;</span>'
        ));
}

$query_args = array(
	'post_type' => 'proiecte',
	'posts_per_page' => -1,
	'order' => 'ASC',
	'orderby' => 'title',
	// 'meta_key' => 'ordine_prezentare'
);
$query = new WP_Query($query_args);

global $wpdb;

if( $query->have_posts() ) {
	$query_anyvote = "SELECT COUNT(*) FROM hack_qrcodes WHERE voted";

	if($wpdb->get_var($query_anyvote)) {
		echo '<h3>Rezultatele votului de popularitate:</h3>';
		echo '<div id="charts">';

		$query_order = array(
			'post_type' => 'proiecte',
			'posts_per_page' => -1,
			'order' => 'DESC',
			'orderby' => 'meta_value_num',
			'meta_key' => 'proiect_total_puncte'
		);
		$order = new WP_Query($query_order);

		while( $order->have_posts() ) {
			$order->the_post();

			$prezent_la_vot = get_field('vot_popularitate');

			if( $prezent_la_vot ) {
				printf('<div class="bar_container"><div class="title"><span>%1$s</span><div class="points">%2$s</div></div><div class="bar" data-width="%2$s"></div></div>',
					get_the_title(),
					get_field('proiect_total_puncte')
					);
			}
		}

		echo '</div>';
	}

	echo __('[:ro]<h3>Proiectele înscrise la raspberry hack:</h1>[:en]<h1>Projects registered for the raspberry hack show-off:</h3>');

	echo '<div id="projects_list">';

	while( $query->have_posts() ) {
		$query->the_post();

		$cats = get_the_category();

		printf( '<article class="project"><h1><a href="%1$s" target="_blank">%2$s</a><small>%5$s</small></h1><p class="authors">Autori: %3$s</p><div class="description" style="display:none"><h5>Descriere:</h5>%4$s</div><div class="more"><a href="#">+</a></div></article>',
			get_field('proiect_link'),
			get_the_title(),
			get_authors_list( get_field('proiect_autori') ),
			__('[:ro]' . get_field('proiect_descriere_ro') . '[:en]' . get_field('proiect_descriere_en') ),
			$cats[0]->cat_name
			);
	}

	echo '</div>';
} else {
	echo __('[:ro]<h3>Deocamdată nu sunt proiecte înscrise.</h1>[:en]<h1>There are no registered projects for now.</h3>');
}