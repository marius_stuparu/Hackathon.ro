<?php
/**
 * @package Hackathon
 * @subpackage Hackover
 */
    $home_args = array(
        'post_type' => 'page',
        'page_id' => 20 );
        
    $home_page = new WP_Query( $home_args );
    
    while ( $home_page->have_posts() ) : $home_page->the_post();
?>
<div id="main_content" class="gold">
    <?php the_content() ?>
</div>
<?php endwhile; ?>

<div id="sidebar">
    <?php get_template_part('sidebar', 'home') ?>
</div>