<?php
/**
 * @package Hackathon
 * @subpackage Hackover
 */

/**
 * Set the content width based on the theme's design and stylesheet.
 *
 * Used to set the width of images and content. Should be equal to the width the theme
 * is designed for, generally via the style.css stylesheet.
 */
if ( ! isset( $content_width ) )
	$content_width = 1000;

get_template_part('nhp', 'options');

require_once 'includes/init.inc.php';
require_once 'includes/security.inc.php';
require_once 'includes/navigation.inc.php';
require_once 'includes/content.inc.php';
require_once 'includes/widgets.inc.php';
require_once 'includes/misc.inc.php';
require_once 'includes/footer.inc.php';
require_once 'includes/post_types.inc.php';