<div class="module large prevs">
    <h4><?php if (function_exists('qtrans_getLanguage')) {
            echo __('[:ro]Edițiile anterioare[:en]Previous editions', 'hackathon');
        } else { echo __('Edițiile anterioare', 'hackathon'); } ?></h4>
    <p class="prev_hackover"><a href="<?php echo get_permalink(862) ?>">raspberry hack</a></p>
    <p class="prev_hackover"><a href="<?php echo get_permalink(566) ?>">HACKOVER 2012</a></p>
    <p><a href="<?php echo get_permalink(564) ?>"><img src="<?php bloginfo( 'template_url' ); ?>/img/banner_iunie.gif" alt="<?php if (function_exists('qtrans_getLanguage')) {
            echo __('[:ro]Hackathon iunie 2012[:en]Hackathon June 2012', 'hackathon');
        } else { echo __('Hackathon iunie 2012', 'hackathon'); } ?>" /></a></p>
</div>

<div class="module small">
    <h5><a href="https://www.facebook.com/hackathon.ro" target="_blank"><?php if (function_exists('qtrans_getLanguage')) {
            echo __('[:ro]Urmărește-ne pe Facebook[:en]Follow us on Facebook', 'hackathon');
        } else { echo __('Urmărește-ne pe Facebook', 'hackathon'); } ?></a></h5>
</div>

<?php dynamic_sidebar( 'home-widget-area' ) ?>

<div class="module large">
    <h4><?php if (function_exists('qtrans_getLanguage')) {
            echo __('[:ro]Partenerii acestei ediții:[:en]Parteners for this edition:', 'hackathon');
        } else { echo __('Partenerii acestei ediții:', 'hackathon'); } ?></h4>
    <?php
        $num_parteneri = 6;
        $parteneri_logo = array();
        $parteneri_link = array();

        $options = get_option('hackathon');

        for ( $iter = 1; $iter <= $num_parteneri; $iter++ ) {
            if ( $options['checkbox_show_partner_' . $iter] ) {
                $parteneri_logo[] = $options['logo_partner_' . $iter];
                $parteneri_link[] = $options['url_partner_' . $iter];
            }
        }

        for ( $partner = 0; $partner < count($parteneri_logo); $partner++ ) {
            echo '<p class="parteneri"><a href="' . $parteneri_link[$partner] . '" class="sustinatori" rel="nofollow" target="_blank"><img src="' . $parteneri_logo[$partner] . '" /></a>';
        }
    ?>
</div>