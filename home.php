<?php
/**
 * The home template file.
 *
 * @package WordPress
 * @subpackage Hackathon
 */

get_header();

get_template_part('content', 'home');

get_footer();
?>