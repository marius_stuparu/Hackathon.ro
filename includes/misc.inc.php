<?php
if ( ! function_exists( 'return_page_name' ) ) :
function return_page_name(){
	if(is_front_page()) return 'home';
	
	return get_page(get_query_var( 'page_id' ))->post_name;
}
endif;

function custom_admin_menu() {
     remove_menu_page('edit-comments.php');
	 remove_menu_page('link-manager.php');
}

function checkPage() {
	$return = 1;

	if (is_page(260)) {$return = 0;}
    if (is_page(590)) {$return = 0;}
    if (is_page(314)) {$return = 0;}
    if (is_page(767)) {$return = 0;}
    if (is_page(773)) {$return = 0;}

    return $return;
}

add_filter('body_class','add_language_classes');
function add_language_classes($classes) {
	if( function_exists('qtrans_getLanguage') ) {
		$lang = qtrans_getLanguage();

		$classes[] = 'lang-' . $lang;
	}
	
	// return the $classes array
	return $classes;
}

add_filter('body_class','page_specifiv_classes');
function page_specifiv_classes($classes) {
	if( is_page(764) ) {
		$classes[] = 'lista_proiecte';
	}

	if( is_page(767) ) {
		$classes[] = 'voting_proiecte';
	}
	
	// return the $classes array
	return $classes;
}