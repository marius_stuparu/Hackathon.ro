<?php
/**
 * Sets the post excerpt length to 50 characters.
 * @return int
 */
function hackathon_excerpt_length( $length ) {
	return 30;
}
add_filter( 'excerpt_length', 'hackathon_excerpt_length' );

/**
 * Returns a "Continue Reading" link for excerpts
 * @return string "Continue Reading" link
 */
function hackathon_continue_reading_link() {
	return '<p class="read_more"><a href="'. get_permalink() . '">' . __( 'Citește articolul <span class="meta-nav">&raquo;</span>', 'hackathon' ) . '</a></p>';
}

/**
 * Replaces "[...]" (appended to automatically generated excerpts) with an ellipsis and hackathon_continue_reading_link().
 * @return string An ellipsis
 */
function hackathon_auto_excerpt_more( $more ) {
	return ' &hellip;' . hackathon_continue_reading_link();
}
add_filter( 'excerpt_more', 'hackathon_auto_excerpt_more' );

/**
 * Adds a pretty "Continue Reading" link to custom post excerpts.
 * @return string Excerpt with a pretty "Continue Reading" link
 */
function hackathon_custom_excerpt_more( $output ) {
	if ( has_excerpt() && ! is_attachment() ) {
		$output .= hackathon_continue_reading_link();
	}
	return $output;
}
add_filter( 'get_the_excerpt', 'hackathon_custom_excerpt_more' );

if ( ! function_exists( 'hackathon_posted_on' ) ) :
/**
 * Prints HTML with meta information for the current post-date/time and author.
 */
function hackathon_posted_on() {
	printf( __( '<a class="meta-prep" href="%1$s" title="%2$s" rel="bookmark"><span class="entry-date">%3$s</span></a>', 'hackathon' ),
		get_permalink(),
		esc_attr( get_the_time() ),
		get_the_date()
	);
}
endif;

if ( ! function_exists( 'hackathon_posted_on_return' ) ) :
/**
 * Prints HTML with meta information for the current post-date/time and author.
 */
function hackathon_posted_on_return() {
	return __( '<a class="meta-prep" href="' . get_permalink() . '" title="' . esc_attr( get_the_time() ) . '" rel="bookmark"><span class="entry-date">' . get_the_date() . '</span></a>', 'hackathon' );
}
endif;

function get_authors_list( $raw_text ) {
	$split = preg_split('/,(\s)*([\n\r]*)/', $raw_text);
	$return_string = '';

	foreach ($split as $name) {
		$return_string .= '<span class="name">' . $name . '</span>&nbsp;';
	}

	return $return_string;
}

function get_thumbnail( $id ) {
	if( has_post_thumbnail($id) ) {
		return get_the_post_thumbnail($id);
	} else {
		return get_bloginfo('template_url') . '/img/raspberry-pi-thumbnail.jpg';
	}
}