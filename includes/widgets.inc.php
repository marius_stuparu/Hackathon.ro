<?php
/**
 * Register widgetized areas.
 * @uses register_sidebar
 */
function hackathon_widgets_init() {
	register_sidebar( array(
        'name' => __( 'Sidebar home', 'hackathon' ),
        'id' => 'home-widget-area',
        'description' => __( 'Bagă acilea widgeturi pentru home', 'hackathon' ),
        'before_widget' => '<div id="%1$s" class="module large %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h4 class="widget-title">',
        'after_title' => '</h4>',
    ) );

	register_sidebar( array(
		'name' => __( 'Sidebar pagini', 'hackathon' ),
		'id' => 'primary-widget-area',
		'description' => __( 'Bagă acilea widgeturi pt pagini', 'hackathon' ),
		'before_widget' => '<div id="%1$s" class="module large %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widget-title">',
		'after_title' => '</h4>',
	) );
}
/** Register sidebars by running hackathon_widgets_init() on the widgets_init hook. */
add_action( 'widgets_init', 'hackathon_widgets_init' );