<?php
function checkValidVote($check_qr, $vote_1, $vote_2, $vote_3) {
	$qr_valid = false;
	$vote_valid = false;

	global $wpdb;

	if( preg_match('/21042013(\d){3}hackathon/', $check_qr) ) { // Safety first
		$query_search = "SELECT COUNT(*) FROM hack_qrcodes WHERE code='" . $check_qr . "' AND !voted";
		
		if( $wpdb->get_var($query_search) ) {
			$qr_valid = true;
		}
	}

	if( ($vote_1 !== $vote_2) && ($vote_1 !== $vote_3) && ($vote_2 !== $vote_3) ) {
		$vote_valid = true;
	}

	return ($qr_valid && $vote_valid);
}

function register_vote() {
	$the_vote = $_POST;
	unset($_POST);
	
	global $wpdb;

	// Some basic protection
	$qrcode = substr($the_vote['qrcode'], 0, 20);
	$vote_1 = (int)substr($the_vote['vote_1'], 0, 6);
	$vote_2 = (int)substr($the_vote['vote_2'], 0, 6);
	$vote_3 = (int)substr($the_vote['vote_3'], 0, 6);

	if( checkValidVote($qrcode, $vote_1, $vote_2, $vote_3) ) {
		$query_mark_qr = "UPDATE hack_qrcodes SET voted=true WHERE code='" . $qrcode . "'";
		
		if( $wpdb->query($query_mark_qr) ) {
			// Vote 1
			$tot_points_1 = get_field('proiect_total_puncte', $vote_1);
			$sum_votes_1 = get_field('proiect_voturi_loc_1', $vote_1);

			update_field('proiect_total_puncte', ($tot_points_1 + 15), $vote_1);
			update_field('proiect_voturi_loc_1', ($sum_votes_1 + 1), $vote_1);

			// Vote 2
			$tot_points_2 = get_field('proiect_total_puncte', $vote_2);
			$sum_votes_2 = get_field('proiect_voturi_loc_2', $vote_2);

			update_field('proiect_total_puncte', ($tot_points_2 + 10), $vote_2);
			update_field('proiect_voturi_loc_2', ($sum_votes_2 + 1), $vote_2);

			// Vote 3
			$tot_points_3 = get_field('proiect_total_puncte', $vote_3);
			$sum_votes_3 = get_field('proiect_voturi_loc_3', $vote_3);

			update_field('proiect_total_puncte', ($tot_points_3 + 5), $vote_3);
			update_field('proiect_voturi_loc_3', ($sum_votes_3 + 1), $vote_3);

			// Thanks
			echo __('[:ro]<h2 class="title">Votul tău a fost înregistrat. Mulțumim!</h2><p class="details">Pornește scanner pentru un vot nou.</p>[:en]<h2 class="title">Your vote has been registered. Thank you!</h2><p class="details">Start the scanner scanner for a new vote.</p>');
			echo __('[:ro]<p class="backlink"><a href="' . get_permalink(665) . '">Pornește QR scanner</a></p>[:en]<p class="backlink"><a href="' . get_permalink(665) . '">Back to the QR scanner</a></p>');
		}
	} else {
		echo __('[:ro]<h2 class="title">EROARE! Votul nu a fost înregistrat.</h2><p class="details">Codul QR este invalid sau a mai fost folosit, sau proiectele au fost identificate greșit.</p><p class="details">Dacă ai încercat să fraudezi, rușinică!</p>[:en]<h2 class="title">ERROR! Your vote has not been registered.</h2><p class="details">The QR code is invalid or has been used already, or the projects have not been correctly identified</p><p class="details">If you tried to cheat, shame on you!</p>');
		echo __('[:ro]<p class="backlink"><a href="' . get_permalink(665) . '">Înapoi la QR scanner</a></p>[:en]<p class="backlink"><a href="' . get_permalink(665) . '">Back to the QR scanner</a></p>');
	}
}

register_vote();