<?php
echo __('[:ro]<h2 class="title">Votează cel mai bun proiect la raspberry hack:</h2>[:en]<h2 class="title">Vote the best project at raspberry hack:</h2>');
echo __('[:ro]<h4 class="subtitle">Pentru a vota, scanează codul QR pe care l-ai primit.</h4>[:en]<h4 class="subtitle">To vote, scan the QR code that you have reveived.</h4>');
echo __('[:ro]<p class="details">După acceptarea codului, desprinde partea de cod și las-o la unul din Helpări.</p>[:en]<p class="details">After your code has been accepted, tear up the QR code and leave it to one of the Helpers.</p>');

echo '<div id="scanner"><video id="qr-video" width="320" height="240"></video><canvas style="visibility:hidden" id="qr-canvas" width="320" height="240"></canvas></div>';