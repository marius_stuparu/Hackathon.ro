<?php
function post_type_projects() {
    $args = array(
        'labels' => array(
        	'name' 					=> __('Proiecte înscrise'),
        	'singular_name' 		=> __('Proiect'),
        	'menu_name' 			=> __('Proiecte'),
        	'all_items'				=> __('Toate proiectele'),
        	'add_new' 				=> __('Adaugă'),
        	'add_new_item' 			=> __('Adaugă proiect'),
        	'edit_item' 			=> __('Modifică proiect'),
        	'new_item' 				=> __('Proiect nou'),
        	'view_item' 			=> __('Vezi proiect'),
        	'search_items'			=> __('Caută proiecte'),
        	'not_found' 			=> __('Niciun proiect publicat'),
        	'not_found_in_trash' 	=> __('Niciun proiect în coș')
        	),
        'description' 			=> __('Proiecte înscrise la show-off raspberry hack'),
        'public' 				=> true,
        'exclude_from_search' 	=> true,
        'show_ui' 				=> true,
        'show_in_nav_menus' 	=> false,
        'show_in_menu' 			=> true,
        'show_in_admin_bar' 	=> false,
        'menu_position' 		=> 5,
        'menu_icon' 			=> null,
        'capability_type' 		=> 'post',
        'map_meta_cap' 			=> true,
        'hierarchical' 			=> false,
        'supports' 				=> array(
        	'title',
			'editor',
			'thumbnail', // current theme must also support post-thumbnails
			'excerpt',
			'custom-fields',
        	),
        'taxonomies' 			=> array('category', 'post_tag'),
        'has_archive' 			=> true,
        'rewrite' 				=> array(
        	'slug' => __('proiecte'),
        	'with_front' 		=> false,
        	'feeds' 			=> false,
        	'pages' 			=> true,
        	),
    );
    register_post_type( 'proiecte' , $args );
}

add_action('init', 'post_type_projects');

add_filter( 'manage_edit-proiecte_columns', 'projects_list_columns' ) ;

function projects_list_columns( $columns ) {

    $columns = array(
        'cb' => '<input type="checkbox" />',
        'title' => __( 'Titlu' ),
        'categories' => __('Categorie'),
        'popularitate' => __( 'Vot popularitate' ),
        'puncte' => __( 'Puncte' ),
        'voturi_1' => __( 'Voturi 1' ),
        'voturi_2' => __( 'Voturi 2' ),
        'voturi_3' => __( 'Voturi 3' )
    );

    return $columns;
}

add_action( 'manage_proiecte_posts_custom_column', 'manage_columns', 10, 2 );

function manage_columns( $column, $post_id ) {
    global $post;

    switch($column) {
        case 'puncte' :
            $puncte = get_field( 'proiect_total_puncte', $post_id );

            if ( empty( $puncte ) )
                echo __( 0 );

            else
                echo $puncte;

            break;

        case 'voturi_1' :
            $voturi_1 = get_field( 'proiect_voturi_loc_1', $post_id );

            if ( empty( $voturi_1 ) )
                echo __( 0 );

            else
                echo $voturi_1;

            break;

        case 'voturi_2' :
            $voturi_2 = get_field( 'proiect_voturi_loc_2', $post_id );

            if ( empty( $voturi_2 ) )
                echo __( 0 );

            else
                echo $voturi_2;

            break;

        case 'voturi_3' :
            $voturi_3 = get_field( 'proiect_voturi_loc_3', $post_id );

            if ( empty( $voturi_3 ) )
                echo __( 0 );

            else
                echo $voturi_3;

            break;

        case 'popularitate' :
            $popularitate = get_field( 'vot_popularitate', $post_id );

            if ( $popularitate )
                echo '<input type="checkbox" checked="checked" disabled />';
            else
                echo '<input type="checkbox" disabled />';

            break;

        default :
            break;
    }
}

add_filter( 'manage_edit-proiecte_sortable_columns', 'sortable_columns' );

function sortable_columns( $columns ) {

    $columns['categories'] = 'categories';
    $columns['puncte'] = 'puncte';
    $columns['voturi_1'] = 'voturi_1';
    $columns['popularitate'] = 'popularitate';

    return $columns;
}