<?php
/**
 * Get our wp_nav_menu() fallback, wp_page_menu(), to show a home link.
 */
function hackathon_page_menu_args( $args ) {
	$args['show_home'] = true;
	return $args;
}
add_filter( 'wp_page_menu_args', 'hackathon_page_menu_args' );

if (function_exists('qtrans_convertURL')) {
	function qtrans_in_nav_el($output, $item, $depth, $args) {
		$attributes = !empty($item->attr_title) ? ' title="' . esc_attr($item->attr_title) . '"' : '';
		$attributes .=!empty($item->target) ? ' target="' . esc_attr($item->target) . '"' : '';
		$attributes .=!empty($item->xfn) ? ' rel="' . esc_attr($item->xfn) . '"' : '';

		// Integration with qTranslate Plugin
		$attributes .=!empty($item->url) ? ' href="' . esc_attr( qtrans_convertURL($item->url) ) . '"' : '';

		$output = $args->before;
		$output .= '<a' . $attributes . '>';
		$output .= $args->link_before . apply_filters('the_title', $item->title, $item->ID) . $args->link_after;
		$output .= '</a>';
		$output .= $args->after;

		return $output;
	}

	add_filter('walker_nav_menu_start_el', 'qtrans_in_nav_el', 10, 4);
}