<?php
/**
 * Check if the code is valid and has not voted yet
 * @param  string  $qr_code scanned code
 * @return boolean          return true if code is valid and has not voted yet
 */
function checkValidCode($check_qr) {
	global $wpdb;

	if( preg_match('/21042013(\d){3}hackathon/', $check_qr) ) { // Safety first
		$query_search = "SELECT COUNT(*) FROM hack_qrcodes WHERE code='" . $check_qr . "' AND !voted";
		
		if( $wpdb->get_var($query_search) ) {
			return true;
		} else {
			return false;
		}
	} else {
		return false;
	}
}

function get_votes() {
	// Get the QR code
	$qr_code = substr($_GET['i'], 0, 20);
	// Then unset GET for safety
	unset($_GET);

	if ( null !== $qr_code ) {
		if( checkValidCode($qr_code) ) {
			$query_args = array(
				'post_type' => 'proiecte',
				'posts_per_page' => -1,
				'order' => 'ASC',
				'orderby' => 'meta_value_num',
				'meta_key' => 'ordine_prezentare'
			);
			$query = new WP_Query($query_args);

			if( $query->have_posts() ) {
				echo __('[:ro]<h2 class="title">Votează cel mai bun proiect la raspberry hack:</h2>[:en]<h2 class="title">Vote the best project at raspberry hack:</h2>');
				echo __('[:ro]<p class="details">Te rugăm să acorzi toate cele 3 voturi. Locul 1 înseamnă punctaj maxim, locul 3 punctaj minim. Un proiect poate avea un singur vot.</p>[:en]<p class="details">Please award all three places. 1<sup>st</sup> place awards most points, 3<sup>rd</sup> place awards the least points. A project can have only one vote.</p>');

				echo '<form id="projects_list" action="' . get_permalink(767) . '" method="post"><fieldset>';
				echo __('[:ro]<p id="reset"><span>Resetează voturile</span></p>[:en]<p id="reset"><button>Reset votes</button></p>');

				// Dsiplay all projects
				while( $query->have_posts() ) {
					$query->the_post();

					$cats = get_the_category();

					printf( '<article class="project"><h1>%1$s&nbsp;(<small>%11$s</small>)</h1><p class="authors">Autori: %2$s</p><div class="description" style="display:none"><h5>Detalii:</h5>%3$s</div><div class="more"><a href="#">+</a></div><div class="votes"><span>%4$s</span><label for="%5$s"><input id="%5$s" type="radio" name="vote_1" value="%6$s" />1</label><label for="%7$s"><input id="%7$s" type="radio" name="vote_2" value="%8$s" />2</label><label for="%9$s"><input id="%9$s" type="radio" name="vote_3" value="%10$s" />3</label></div></article>',
						get_the_title(),
						get_authors_list( get_field('proiect_autori') ),
						__('[:ro]' . get_field('proiect_descriere_ro') . '[:en]' . get_field('proiect_descriere_en') ),
						__('[:ro]Votează locul:[:en]Vote the place'),
						'project_' . get_the_ID() . '_1',
						get_the_ID(),
						'project_' . get_the_ID() . '_2',
						get_the_ID(),
						'project_' . get_the_ID() . '_3',
						get_the_ID(),
						$cats[0]->cat_name
					);
				}

				echo '<input type="hidden" name="qrcode" value="' . $qr_code . '" /><input type="hidden" name="a" value="vote" />';
				echo '</fieldset><fieldset>';
				echo __('[:ro]<p><input type="submit" value="Votează" name="submit" disabled /></p>[:en]<p><input type="submit" value="Vote" name="submit" disabled /></p>');
				echo '</fieldset></form>';
			} else {
				echo __('[:ro]<h3>Deocamdată nu sunt proiecte înscrise.</h1>[:en]<h1>There are no registered projects for now.</h3>');
			}
		} else {
			echo __('[:ro]<h3 class="title">Ne pare rău, dar acest cod este invalid sau a mai fost folosit pentru vot!</h3>[:en]<h3 class="title">We are sorry, but this code is invalid or has already been used!</h3>'); 
			echo __('[:ro]<p class="backlink"><a href="' . get_permalink(767) . '">Înapoi la QR scanner</a></p>[:en]<p class="backlink"><a href="' . get_permalink(767) . '">Back to the QR scanner</a></p>');
		}
	} else {
		echo __('[:ro]<h3 class="title">Codul nu a fost detectat. Cum ai ajuns aici?</h3>[:en]<h3 class="title">The code has not been detected. How did you get here?</h3>'); 
		echo __('[:ro]<p class="backlink"><a href="' . get_permalink(767) . '">Înapoi la QR scanner</a></p>[:en]<p class="backlink"><a href="' . get_permalink(767) . '">Back to the QR scanner</a></p>');
	}
}

get_votes();