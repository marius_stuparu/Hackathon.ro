<?php
function hackathon_copyright(){
    $current_date = getdate();
    $current_year = $current_date['year'];
    $copyright = 'Copyright &copy; ';
    
    if ($current_year == 2012){
        $copyright .= '2012' . ' | ' .get_bloginfo('name');
    } else {
        $copyright .= '2012-' . $current_year . ' | ' .get_bloginfo('name');
    }

    if (function_exists('qtrans_getLanguage')) {
        $lang = qtrans_getLanguage();

        if ($lang == 'ro') {
            $copyright .= '. Toate drepturile rezervate.';
            $copyright .= ' <a href="' . get_permalink(41) . '">Termeni și condiții</a>';
        }

        if ($lang == 'en') {
            $copyright .= '. All rights reserved';
            $copyright .= ' <a href="' . get_permalink(41) . '">Terms and conditions</a>';
        }
        
    } else {
        $copyright .= '. Toate drepturile rezervate.';
        $copyright .= ' <a href="' . get_permalink(41) . '">Termeni și condiții</a>';
    }

    return $copyright;
}