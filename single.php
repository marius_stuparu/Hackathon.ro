<?php
/**
 * The Template for displaying all single posts.
 *
 * @package WordPress
 * @subpackage PowerConsult
 */

get_header();

get_template_part( 'content', 'single' );

get_footer();
?>