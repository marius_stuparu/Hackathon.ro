<?php
/**
 * The template for displaying Archive pages.
 *
 * @package WordPress
 * @subpackage PowerConsult
 */

get_header();

get_template_part( 'loop', 'archive' );

get_footer(); ?>
